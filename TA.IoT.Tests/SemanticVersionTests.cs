﻿using System.Linq;
using Machine.Specifications;
using Xunit;

namespace TA.IoT.Tests
{
    public class SemanticVersionTests
    {
        [Fact]
        void TwoInstancesWithTheSameVersionNumberAreEqual()
        {
            var version1 = new SemanticVersion("1.2.3");
            var version2 = new SemanticVersion(1, 2, 3);
            version1.ShouldEqual(version2);
            (version1 == version2).ShouldBeTrue();
        }

        [Fact]
        void ConstructorCanParseAFullySpecifiedSemanticVersion()
        {
            var fullVersionString = "001.002.003-PrereleaseLabel.456+BuildLabel.789";
            var version = new SemanticVersion(fullVersionString);
            version.MajorVersion.ShouldEqual(1);
            version.MinorVersion.ShouldEqual(2);
            version.PatchVersion.ShouldEqual(3);
            version.PrereleaseVersion.Any().ShouldBeTrue();
            version.PrereleaseVersion.Single().ShouldEqual("PrereleaseLabel.456");
            version.BuildVersion.Any().ShouldBeTrue();
            version.BuildVersion.Single().ShouldEqual("BuildLabel.789");
        }

        [Fact]
        void PrereleaseVersionSortsBeforeReleseVersion()
        {
            var prerelease = new SemanticVersion("1.2.3-Beta.1");
            var release = new SemanticVersion("1.2.3");
            prerelease.ShouldBeLessThan(release);
            release.ShouldBeGreaterThan(prerelease);
        }

        [Fact]
        void EqualityRequiresAllComponentsToMatchExactly()
        {
            var prerelease = new SemanticVersion("1.2.3-Beta.1");
            var prereleaseBuild37 = new SemanticVersion("1.2.3-Beta.1+build.37");
            var prereleaseBuild38 = new SemanticVersion("1.2.3-Beta.1+build.38");
            var release = new SemanticVersion("1.2.3");
            release.ShouldNotEqual(prerelease);
            prerelease.ShouldNotEqual(prereleaseBuild37);
            prereleaseBuild37.ShouldNotEqual(prereleaseBuild38);
        }
    }
}
