// This file is part of the TA.IoT project
// 
// File: Timeout.cs  Created: 2017-07-26@02:04
// Last modified: 2017-07-28@02:37

using System;

namespace TA.IoT
    {
    /// <summary>
    ///     Encapsulates the concept of a time-out (or time interval) which builds upon
    ///     <see cref="System.TimeSpan" />. Implicit conversions are provided for use when ticks or
    ///     milliseconds are needed. For example, some timers take a TimeSpan, some take
    ///     milliseconds and still others take a number of system ticks. A Timeout can be
    ///     constructed from or cast to any of those types. Instances of this type are immutable.
    /// </summary>
    public struct Timeout
        {
        /// <summary>
        ///     Gets the timeout as a <see cref="TimeSpan" />.
        /// </summary>
        public TimeSpan TimeSpan { get; }

        /// <summary>
        ///     Gets the timeout expressed in milliseconds.
        /// </summary>
        public int Milliseconds => (int) (TimeSpan.Ticks / TimeSpan.TicksPerMillisecond);

        /// <summary>
        ///     Gets the timeout expressed in system clock ticks.
        /// </summary>
        public long Ticks => TimeSpan.Ticks;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Timeout" /> class froma a <see cref="TimeSpan" />.
        /// </summary>
        /// <param name="duration">The timeout duration.</param>
        private Timeout(TimeSpan duration) : this()
            {
            TimeSpan = duration;
            }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Timeout" /> class from the specified number of milliseconds.
        /// </summary>
        /// <param name="milliseconds">The timeout duration, in milliseconds.</param>
        private Timeout(int milliseconds) : this()
            {
            TimeSpan = TimeSpan.FromTicks(milliseconds * TimeSpan.TicksPerMillisecond);
            }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Timeout" /> class from a number of clock ticks.
        /// </summary>
        /// <param name="ticks">The timeout duration in system clock ticks.</param>
        private Timeout(long ticks) : this()
            {
            TimeSpan = TimeSpan.FromTicks(ticks);
            }

        /// <summary>
        ///     Gets a <see cref="Timeout" /> set to the specified number of milliseconds.
        /// </summary>
        /// <param name="milliseconds">The timeout interval, in whole milliseconds.</param>
        public static Timeout FromMilliseconds(int milliseconds) => new Timeout(milliseconds);

        /// <summary>
        ///     Gets a <see cref="Timeout" /> set to the specified number of system clock ticks.
        /// </summary>
        /// <param name="ticks">The ticks.</param>
        /// <returns>Timeout.</returns>
        public static Timeout FromTicks(long ticks) => new Timeout(ticks);

        /// <summary>
        ///     Gets a <see cref="Timeout" /> set to the specified interval.
        /// </summary>
        /// <param name="span">The timeout duration, as a <see cref="TimeSpan" />.</param>
        /// <returns>Timeout.</returns>
        public static Timeout FromTimeSpan(TimeSpan span) => new Timeout(span);

        /// <summary>
        ///     Gets a <see cref="Timeout" /> set to the specified number of whole seconds.
        /// </summary>
        /// <param name="seconds">The timeout interval, in seconds.</param>
        /// <returns>Timeout.</returns>
        public static Timeout FromSeconds(int seconds) => FromMilliseconds(1000 * seconds);

        /// <summary>
        ///     Gets a <see cref="Timeout" /> set to the specified number of fractional seconds.
        /// </summary>
        /// <param name="seconds">The timeout period, in seconds.</param>
        /// <returns>Timeout.</returns>
        public static Timeout FromSeconds(double seconds) => FromTicks((long) (TimeSpan.TicksPerSecond * seconds));


        /// <summary>
        ///     Performs an implicit conversion from <see cref="Timeout" /> to <see cref="TimeSpan" />.
        /// </summary>
        /// <param name="t">The t.</param>
        /// <returns>The timeout value converted to a <see cref="TimeSpan" />.</returns>
        public static implicit operator TimeSpan(Timeout t) => t.TimeSpan;

        /// <summary>
        ///     Performs an implicit conversion from <see cref="Timeout" /> to <see cref="System.Int32" />.
        /// </summary>
        /// <param name="t">The t.</param>
        /// <returns>The timeout value converted to milliseconds.</returns>
        public static implicit operator int(Timeout t) => t.Milliseconds;

        /// <summary>
        ///     Performs an implicit conversion from <see cref="Timeout" /> to <see cref="System.Int64" />.
        /// </summary>
        /// <param name="t">The timeout instance.</param>
        /// <returns>The timeout value expressed in system clock ticks.</returns>
        public static implicit operator long(Timeout t) => t.Ticks;

        /// <summary>
        ///     Implements the + operator.
        /// </summary>
        /// <param name="lhs">The LHS.</param>
        /// <param name="rhs">The RHS.</param>
        /// <returns>The result of the operator.</returns>
        public static Timeout operator +(Timeout lhs, Timeout rhs) => lhs.Plus(rhs);

        /// <summary>
        ///     Returns a new instance with the combined interval of this instance and another instance.
        /// </summary>
        /// <param name="other">The other instance.</param>
        /// <returns>A new Timeout instance containing the combined time span.</returns>
        public Timeout Plus(Timeout other) => new Timeout(TimeSpan + other.TimeSpan);

        /// <summary>
        ///     Gets a <see cref="Timeout" /> structure initialized to zero.
        /// </summary>
        /// <value>An instance of <see cref="Timeout" /> initialized to zero ticks.</value>
        public static Timeout Zero => FromTicks(0);

        public static Timeout Infinite => FromTimeSpan(System.Threading.Timeout.InfiniteTimeSpan);

        public override string ToString() => TimeSpan.ToString();
        }
    }