﻿// This file is part of the TA.IoT project
// 
// File: Crc8.cs  Created: 2017-07-26@19:32
// Last modified: 2017-07-28@02:38

using System.Linq;

namespace TA.IoT
    {
    /// <summary>
    ///     Compute CRC-8 checksums.
    /// </summary>
    /// <remarks>
    ///     This implementation is based on one found at
    ///     http://www.codeproject.com/Articles/19059/C-CCITT-CRC-Algorithm
    /// </remarks>
    public class Crc8 : IComputeChecsum
        {
        public const int CCITTPolynomial = 0x07;
        public const int DallasMaximPolynomial = 0x31;
        public const int DefaultPolynomial = 0xd5;
        public const int SaeJ1850Polynomial = 0x1D;
        public const int SMBusPolynomial = CCITTPolynomial;
        public const int WCDMAPolynomial = 0x9b;

        public Crc8(int polynomial)
            {
            Table = GeneratePolynomialLookupTable(polynomial);
            }

        public byte[] Table { get; set; } = new byte[256];

        public byte[] Checksum(params byte[] value)
            {
            byte checksum = 0;
            foreach (var b in value)
                checksum = Table[checksum ^ b];
            return new[] {checksum};
            }

        /// <summary>
        ///     Generates the polynomial lookup table suitable for creating and verifying 8-bit Cyclical Redundancy Checksums.
        /// </summary>
        /// <param name="polynomial">
        ///     The polynomial to be used. This varies by implementation and a number of predefined constants
        ///     are provided.
        /// </param>
        /// <returns>A byte[256] array containing the lookup values for the CRC-8 algorithm.</returns>
        public static byte[] GeneratePolynomialLookupTable(int polynomial)
            {
            var csTable = new byte[256];
            for (var i = 0; i < 256; ++i)
                {
                var entry = i;
                for (var j = 0; j < 8; ++j)
                    {
                    if ((entry & 0x80) != 0)
                        entry = (entry << 1) ^ polynomial;
                    else
                        entry <<= 1;
                    }
                csTable[i] = (byte) entry;
                }
            return csTable;
            }

        /// <summary>
        ///     Determines whether the specified string of bytes is valid. The input values should include the checksum
        ///     exactly as received from the wire. Don't strip it off before calling this method.
        /// </summary>
        /// <param name="bytes">
        ///     The bytes to be validated, including the checksum exactly as received from the wire.
        /// </param>
        /// <returns><c>true</c> if the specified input has a valid CRC; otherwise, <c>false</c>.</returns>
        public bool IsValid(params byte[] bytes)
            {
            return Checksum(bytes).All(p => p == 0);
            }
        }
    }