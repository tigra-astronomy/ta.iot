﻿// This file is part of the TA.IoT project
// 
// File: DeviceTransactionTests.cs  Created: 2017-07-28@17:58
// Last modified: 2017-07-28@19:29

using System.Linq;
using Machine.Specifications;
using TA.IoT.Builders;

namespace TA.IoT.Tests
    {
    [Subject(typeof(SimpleTransactionBuilder), "ExpectResponse")]
    public class when_appending_a_single_byte
        {
        Establish context = () => Builder = new SimpleTransactionBuilder();
        Because of = () => Transaction = Builder.Append(0xAA).Build();
        It should_create_a_1_byte_buffer = () => Transaction.TransmitBuffer.Length.ShouldEqual(1);
        It should_not_create_a_receive_buffer = () => Transaction.ReceiveBuffer.Length.ShouldEqual(0);
        It should_populate_the_buffer = () => Transaction.TransmitBuffer[0].ShouldEqual<byte>(0xAA);
        static SimpleTransactionBuilder Builder;
        static IDeviceTransaction Transaction;
        }

    [Subject(typeof(SimpleTransactionBuilder), "ExpectResponse")]
    public class when_expecting_a_response
        {
        Establish context = () => Builder = new SimpleTransactionBuilder();
        Because of = () => Transaction = Builder.ExpectResponse(10).Build();
        It should_clear_the_buffer = () => Transaction.ReceiveBuffer.ShouldEachConformTo(p => p == 0);
        It should_reserve_enough_space_in_the_receive_buffer =
            () => Transaction.ReceiveBuffer.Length.ShouldEqual(10);
        static SimpleTransactionBuilder Builder;
        static IDeviceTransaction Transaction;
        }

    [Subject(typeof(SimpleTransactionBuilder), "Checksum")]
    public class when_appending_a_crc8
        {
        Establish context = () => Builder = new SimpleTransactionBuilder();
        Because of = () => Transaction = Builder
            .Append(0x07, 0xB5, 0xD2, 0x3A)
            .AppendChecksum(new Crc8(Crc8.SMBusPolynomial), new byte[] {0xB4})
            .Build();
        It should_append_the_correct_checksum_bytes = () => Transaction.TransmitBuffer.Last().ShouldEqual<byte>(0x30);
        It should_not_include_preamble_bytes_in_the_buffer = () => Transaction.TransmitBuffer.Length.ShouldEqual(5);
        static SimpleTransactionBuilder Builder;
        static IDeviceTransaction Transaction;
        }

    [Subject(typeof(SimpleTransactionBuilder), "Checksum")]
    public class when_appending_a_multibyte_checksum
        {
        Establish context = () => Builder = new SimpleTransactionBuilder();
        Because of = () => Transaction = Builder
            .Append(0x07, 0xB5, 0xD2, 0x3A)
            .AppendChecksum(new FakeThreeByteChecksum(), new byte[] {0xB4})
            .Build();
        It should_append_the_correct_checksum_bytes = () => Transaction.TransmitBuffer.ShouldEqual(ExpectedResult);
        It should_not_include_preamble_bytes_in_the_buffer = () => Transaction.TransmitBuffer.Length.ShouldEqual(7);
        static SimpleTransactionBuilder Builder;
        static readonly byte[] ExpectedResult = {0x07, 0xB5, 0xD2, 0x3A, 0, 1, 2};
        static IDeviceTransaction Transaction;

        class FakeThreeByteChecksum : IComputeChecsum
            {
            public byte[] Checksum(params byte[] value) => new byte[] {0, 1, 2};
            }
        }
    }