﻿using Machine.Specifications;
using Xunit;

namespace TA.IoT.Tests
{
    public class InterpolatedLookupTests
    {
    [Theory]
    [InlineData(1, 1)]
    [InlineData(2, 2)]
    [InlineData(1.5, 1.5)]
    [InlineData(5, 8)]
    [InlineData(7.5, 75)]
    [InlineData(0, 1)]
    [InlineData(9, 100)]
    public void LookupsShouldReturnInterpolatedValues(double key, double expectedResult)
        {
        var lookupKeys = new double[] {1, 2, 3, 4, 5, 6, 7, 8};
        var lookupValues = new double[] {1, 2, 3, 4, 8, 10, 50, 100};
        var lookup = new InterpolatedLookup(lookupKeys, lookupValues);
        var interpolatedResult = lookup.Lookup(key);
        interpolatedResult.ShouldBeCloseTo(expectedResult, double.Epsilon);
        }
    }
}
