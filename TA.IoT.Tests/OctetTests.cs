﻿using Machine.Specifications;
using Xunit;

namespace TA.IoT.Tests
{
    public class OctetTests
    {
        [Fact]
        public void OctetsOfTheSameValueAreEqual()
        {
            byte byte123 = 123;
            uint uint123 = 123;
            var octet1 = Octet.FromUnsignedInt(uint123);
            var octet2 = (Octet)byte123;
            octet1.ShouldEqual(octet2);
        }

        [Fact]
        public void OctetsAreImmutable()
        {
            var octet1 = Octet.FromInt(127);
            var octet2 = octet1.WithBitSetTo(7, true);
            ReferenceEquals(octet1, octet2).ShouldBeFalse();
            octet1.Equals(octet2).ShouldBeFalse();
            ((int)octet1).ShouldEqual(127);
            ((int)octet2).ShouldEqual(255);
        }

        [Fact]
        public void OctetsCanBeBitShifted()
        {
            var unity = Octet.FromInt(1);
            var duality = unity << 1;
            ((int)duality).ShouldEqual(2);
            var bit7 = Octet.Zero.WithBitSetTo(7, true);
            var bit6 = bit7 >> 1;
            ((int)bit6).ShouldEqual(64);
            duality.ShouldBeOfExactType<Octet>();
            bit6.ShouldBeOfExactType<Octet>();
        }
    }
}
