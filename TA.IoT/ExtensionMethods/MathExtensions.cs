// This file is part of the TA.IoT project
// 
// File: MathExtensions.cs  Created: 2017-07-26@02:05
// Last modified: 2017-07-28@19:52

using System;

namespace TA.IoT.ExtensionMethods
    {
    public static class MathExtensions
        {
        /// <summary>
        ///     Clips (truncates) the input value so that it is within the specified range.
        /// </summary>
        /// <typeparam name="T">Any type that implements <see cref="IComparable" />.</typeparam>
        /// <param name="input">The input.</param>
        /// <param name="upperBound">The upper bound.</param>
        /// <param name="lowerBound">The lower bound.</param>
        /// <returns>
        ///     An instance of <typeparamref name="T" /> that is in the specified range. If
        ///     <typeparamref name="T" /> is a reference type, then the result will be a reference
        ///     to the original input value, the
        ///     <paramref name="upperBound" /> or <paramref name="lowerBound" />.
        /// </returns>
        public static T Clip<T>(this T input, T lowerBound, T upperBound) where T : IComparable
            {
            var result = input.CompareTo(upperBound) > 0 ? upperBound : input;
            result = lowerBound.CompareTo(result) > 0 ? lowerBound : result;
            return result;
            }

        /// <summary>
        ///     Determines whether two floating point values are within the specified tolerance of each
        ///     other.
        /// </summary>
        /// <param name="lhs">The left value (upon which the extension method acts)</param>
        /// <param name="rhs">The right value (passed as a prameter)</param>
        /// <param name="tolerance">
        ///     The tolerance used for comparison. Optional, default is
        ///     <see cref="double.Epsilon" />. Must be a positive nonzero value.
        /// </param>
        /// <returns>
        ///     Returns <c>true</c> if the absolute difference is less than or equal to
        ///     <paramref name="tolerance" />; otherwise returns <c>false</c>.
        /// </returns>
        public static bool IsCloseTo(this double lhs, double rhs, double tolerance = double.Epsilon) =>
            Math.Abs(lhs - rhs) <= tolerance;

        /// <summary>
        ///     Determines whether an object is within a specified range.
        /// </summary>
        /// <typeparam name="T">Any object that implements <see cref="IComparable" />.</typeparam>
        /// <param name="input">The input.</param>
        /// <param name="lowerBound">The inclusive lower bound of the range.</param>
        /// <param name="upperBound">The inclusive upper bound of the range.</param>
        /// <returns><c>true</c> if the input is within the specified range; otherwise, <c>false</c>.</returns>
        public static bool IsInRange<T>(this T input, T lowerBound, T upperBound)
            where T : IComparable => lowerBound.CompareTo(input) <= 0 && upperBound.CompareTo(input) >= 0;

        /// <summary>
        ///     Maps (i.e. scales and translates) a value from a source range to a destination range.
        /// </summary>
        /// <param name="input">The input, which must lie within the source range.</param>
        /// <param name="sourceMinimum">The source range minimum.</param>
        /// <param name="sourceMaximum">The source range maximum.</param>
        /// <param name="destMinimum">The destination range minimum.</param>
        /// <param name="destMaximum">The destination range maximum.</param>
        /// <returns>The mapped value.</returns>
        public static double MapToRange(
            this double input, double sourceMinimum, double sourceMaximum, double destMinimum, double destMaximum)
            {
            var sourceRange = sourceMaximum - sourceMinimum;
            var destRange = destMaximum - destMinimum;
            var sourceAbsoluteOffset = input - sourceMinimum;
            var sourceFraction = sourceAbsoluteOffset / sourceRange;
            var destAbsoluteOffset = destRange * sourceFraction;
            var destResult = destMinimum + destAbsoluteOffset;
            return destResult;
            }
        }
    }