// This file is part of the TA.IoT project
// 
// File: IPeriodicSampling.cs  Created: 2017-07-29@15:07
// Last modified: 2017-07-29@15:11

namespace TA.IoT
    {
    /// <summary>
    ///     An object that is capable of asynchronously and periodically taking samples of some data
    ///     point, such that an output value is updated over time without further intervention.
    /// </summary>
    /// <remarks>
    ///     This behaviour is most usefully combined with the
    ///     <see cref="INotifySensorValueChanged" /> behaviour, but this is neither required nor
    ///     implied.
    /// </remarks>
    public interface IPeriodicSampling
        {
        /// <summary>
        ///     Gets a value indicating whether periodic sampling is active.
        /// </summary>
        /// <value><c>true</c> if periodic sampling is active; otherwise, <c>false</c>.</value>
        bool PeriodicSamplingActive { get; }

        /// <summary>
        ///     Starts sampling the data repeatedly, at the specified interval.
        /// </summary>
        /// <param name="interval">The minimum interval between samples.</param>
        void StartPeriodicSampling(Timeout interval);

        /// <summary>
        ///     Stops periodic sampling.
        /// </summary>
        void StopPeriodicSampling();
        }
    }