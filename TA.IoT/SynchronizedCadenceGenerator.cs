﻿// This file is part of the TA.IoT project
// 
// File: SynchronizedCadenceGenerator.cs  Created: 2017-07-28@23:33
// Last modified: 2017-07-28@23:41

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TA.IoT.ExtensionMethods;

namespace TA.IoT
    {
    /// <summary>
    ///     Manages objects that must be toggled on and off in a regular repeating pattern over
    ///     time. This is known as a cadence. Cadences for all registered items are kept
    ///     synchronized - that is, items with the same cadence will have the same state at the same
    ///     time.
    /// </summary>
    public sealed partial class SynchronizedCadenceGenerator
        {
        /// <summary>
        ///     A timer that triggers updates.
        /// </summary>
        private readonly Timer cadenceTimer;

        /// <summary>
        ///     A list of all the items to be updated when the timer ticks.
        /// </summary>
        private readonly List<CadencedItem> updateList = new List<CadencedItem>();
        /// <summary>
        ///     Protects the UpdateList from race conditions
        /// </summary>
        private readonly ReaderWriterLockSlim updateListLock = new ReaderWriterLockSlim();
        private readonly TimeSpan updatePeriod;

        /// <summary>
        ///     Indicates the current bit position within the cadence register.
        /// </summary>
        internal int CadenceBitPosition;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SynchronizedCadenceGenerator" /> class.
        /// </summary>
        /// <param name="updatePeriod">
        ///     The update period for the cadence timer. Each timer tick represents one bit in the
        ///     cadence pattern, which is stored as a 64-bit unsigned integer. Therefore the time
        ///     for one complete cadence cycle is <c />64 x updatePeriod<c></c>. A reasonable
        ///     starting point is 100 milliseconds.
        /// </param>
        public SynchronizedCadenceGenerator(TimeSpan updatePeriod)
            {
            this.updatePeriod = updatePeriod;
            updateList.Clear();
            cadenceTimer = new Timer(TimerTick, null, Timeout.Infinite.TimeSpan, Timeout.Infinite.TimeSpan);
            }


        /// <summary>
        ///     Adds an item to the cadence manager update list.
        /// </summary>
        /// <param name="turnOn">Action to be invoked when the cadence state becomes active.</param>
        /// <param name="turnOff">Action to be invoked when the cadence state becomes inactive.</param>
        /// <param name="initialCadence">The initial cadence to be used for the first update cycle.</param>
        /// <param name="continuingCadence">Teh cadence used for the second and subsequent update cycles.</param>
        /// <param name="captureSyncContext">
        ///     If <c>true</c> (the default) then actions will be invoked on the current synchronization context;
        ///     if <c>false</c> then actions will be invoked on the thread pool.
        /// </param>
        /// <param name="name">
        ///     A name which uniquely identifies the cadenced item and which can be used to later remove the item
        ///     from the update list. If <c>null</c> is passed then a unique name is generated internally.
        /// </param>
        /// <returns>The unique name of the cadenced item which can be later used to remove the item from the update list.</returns>
        public string Add(Action turnOn, Action turnOff, ulong initialCadence, ulong continuingCadence,
            bool captureSyncContext = true, string name = null)
            {
            var item = new CadencedItem(turnOn, turnOff, initialCadence, continuingCadence, captureSyncContext, name);
            updateListLock.EnterUpgradeableReadLock();
            try
                {
                if (updateList.Any(p => p.Name == item.Name))
                    throw new ArgumentException(
                        "An item with the same name already exists; all items must have unique names");
                updateListLock.EnterWriteLock();
                try
                    {
                    updateList.Add(item);
                    // If this is the first instance, then create and start the timer.
                    if (updateList.Count == 1)
                        StartTimer();
                    return item.Name;
                    }
                finally
                    {
                    updateListLock.ExitWriteLock();
                    }
                }
            finally
                {
                updateListLock.ExitUpgradeableReadLock();
                }
            }

        private Task CadenceUpdate(CadencedItem item, bool state)
            {
            if (state == item.CurrentState)
                return Task.CompletedTask;
            var action = state ? item.TurnOn : item.TurnOff;
            item.CurrentState = state;
            return Task.Factory.StartNew(action, CancellationToken.None, TaskCreationOptions.None, item.Scheduler);
            }


        /// <summary>
        ///     Removes a control from the <see cref="updateList" />.
        ///     If no managed controls remain in the list, then the update timer is stopped.
        /// </summary>
        public void Remove(string itemId)
            {
            updateListLock.EnterUpgradeableReadLock();
            try
                {
                if (updateList.Any(p => p.Name == itemId))
                    {
                    var item = updateList.Single(p => p.Name == itemId);
                    updateListLock.EnterWriteLock();
                    try
                        {
                        updateList.Remove(item);
                        // Stop the update timer if there are no items left in the update list.
                        if (updateList.Count == 0)
                            StopTimer();
                        }
                    finally
                        {
                        updateListLock.ExitWriteLock();
                        }
                    }
                }
            finally
                {
                updateListLock.ExitUpgradeableReadLock();
                }
            }

        private void StartTimer()
            {
            cadenceTimer.Change(updatePeriod, updatePeriod);
            }

        private void StopTimer()
            {
            cadenceTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
            }

        private void TimerTick(object unusedState)
            {
            StopTimer();
            updateListLock.EnterUpgradeableReadLock();
            try
                {
                if (updateList.Count == 0)
                    return;
                // Increment and (if necessary) wrap the cadence bit position index.
                if (++CadenceBitPosition > 63)
                    CadenceBitPosition = 0;
                var updateTasks = new List<Task>();
                updateListLock.EnterWriteLock();
                try
                    {
                    foreach (var cadencedItem in updateList)
                        {
                        if (cadencedItem == null)
                            continue;
                        var state = cadencedItem.Cadence.Bit(CadenceBitPosition);
                        var updateTask = CadenceUpdate(cadencedItem, state);
                        updateTasks.Add(updateTask);
                        }
                    }
                finally
                    {
                    updateListLock.ExitWriteLock();
                    }

                /*
                 * At this point we have a bunch of tasks updating all the cadenced items
                 * possibly in parallel or maybe queued. We should wait for all the updates to complete
                 * before restarting the timer.
                 */

                try
                    {
                    Task.WaitAll(updateTasks.ToArray());
                    }
                catch (Exception)
                    {
                    // ToDo - find a way to remove problem items from the update list
                    }
                finally
                    {
                    StartTimer();
                    }
                }
            finally
                {
                updateListLock.ExitUpgradeableReadLock();
                }
            }
        }
    }