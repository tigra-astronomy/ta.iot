﻿// This file is part of the TA.IoT project
// 
// File: ITransactionBuilder.cs  Created: 2017-07-28@02:31
// Last modified: 2017-07-28@02:34

namespace TA.IoT.Builders
    {
    public interface ITransactionBuilder
        {
        ITransactionBuilder Append(params byte[] bytes);

        ITransactionBuilder AppendChecksum(IComputeChecsum algorithm, byte[] header);

        IDeviceTransaction Build();

        ITransactionBuilder ExpectResponse(int bytes);
        }
    }