﻿// This file is part of the TA.IoT project
// 
// File: ISensorChannel.cs  Created: 2017-07-29@15:53
// Last modified: 2017-07-29@17:43

using System.Threading.Tasks;

namespace TA.IoT
    {
    /// <summary>
    ///     <para>
    ///         This interface represents an abstraction of a sensor (a hardware device) that
    ///         produces a stream of measurements of some physical quantity. Values are given in
    ///         World Units, which by default are in the range 0.0 to 1.0 but scan be mapped to any
    ///         chosen unit scale by setting the <see cref="Scale" /> and <see cref="Offset" />
    ///         properties. The sensor declares it's <see cref="Precision" /> (in scaled World
    ///         Units), which represents the smallest detectable change in the property being
    ///         measured (i.e. the smallest expected change in <see cref="LastValue" />).
    ///     </para>
    ///     <para>
    ///         A sensor channel provides a means of accessing the most recently obtained value
    ///         through the <see cref="LastValue" /> property and a way of asynchronously refreshing
    ///         the value with new data from the sensor, via the <see cref="UpdateValueAsync" />
    ///         method.
    ///     </para>
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         At the hardware level, sensors typically perform some sort of analogue-to-digital
    ///         conversion and return their results in analogue-to-digital-units (ADUs). Since all
    ///         sensors have different ranges, precisions and units this can make it difficult to
    ///         write code that must work with an variety of different devices. The purpose of this
    ///         interface is to provide a standard abstraction over all sensors by enforcing a
    ///         system of World Units.
    ///     </para>
    ///     <para>
    ///         Low level sensor drivers are expected to map the raw ADUs from the hardware into
    ///         Normalized Units in the range 0.0 to 1.0 inclusive, representing the minimum and
    ///         maximum ADU values that can be measured by the sensor.
    ///     </para>
    ///     <para>
    ///         A sensor starts out with <see cref="Scale" /> set to 1.0 and <see cref="Offset" />
    ///         set to 0.0 and in this default state will produce values in Normalized Units. The
    ///         sensor may then be mapped onto any chosen scale of World Units by setting the
    ///         <see cref="Scale" /> and <see cref="Offset" /> properties to appropriately chosen
    ///         values.
    ///     </para>
    ///     <para>
    ///         As an example, consider a temperature sensor. At the device level, the sensor has
    ///         precision 0.1 Kelvin (1 ADU = 0.1 K) and represents Absolute Zero (0K or -273.15°C)
    ///         as 0 ADUs. The sensor has a 12-bit analogue-to-digital converter and can therefore
    ///         distinguish 4,096 distinct values (in the range 0 K to 409.5 K) in steps of 0.1 K.
    ///     </para>
    ///     <para>
    ///         At the sensor device driver level, readings will be mapped so that the sensor
    ///         returns unscaled values in the range 0.0 (representing 0 Kelvin) and 1.0
    ///         (representing 409.5 Kelvin). The unscaled Precision property would return
    ///         approximately 0.00244.
    ///     </para>
    ///     <para>
    ///         To map this sensor onto the Celsius temperature scale, the <see cref="Scale" />
    ///         property would need to be set to <c>409.5</c> and the <see cref="Offset" /> property
    ///         to <c>-273.15</c>. The scaled <see cref="LastValue" /> property would then return
    ///         values in the range -273.15°C and +136.35°C and the scaled
    ///         <see cref="Precision" /> property would return 0.1.
    ///     </para>
    /// </remarks>
    public interface ISensorChannel
        {
        /// <summary>
        ///     Gets the most recently obtained sensor value, in scaled world units.
        /// </summary>
        /// <value>The scaled sensor value <c>(Normalized Value * Scale) + Offset</c>.</value>
        double LastValue { get; }

        /// <summary>
        ///     Gets or sets the offset (in World Units) that is used when mapping Normalized Units to
        ///     World Units. This is typically zero but can be used when a zero reading on the device
        ///     doesn't map to zero in the chosen World Unit scale.
        /// </summary>
        /// <value>The offset.</value>
        double Offset { get; set; }

        /// <summary>
        ///     Gets the precision of the sensor. Precision is the smallest amount by which the value can change
        ///     and is in the range 0.0 to 1.0. The precision is typically inherent to the design of the sensor
        ///     and so is not settable.
        /// </summary>
        /// <value>The precision.</value>
        double Precision { get; }

        /// <summary>
        ///     Gets or sets the sensor scale, which can be used to scale raw sensor readings into world units.
        ///     An example of the use of this would be a temperature probe that returns raw analogue-to-digital
        ///     units. These raw units would be scaled internally by the device driver and then from there can
        ///     easily mapped onto any temperature scale by setting this property.
        ///     If not set, then Scale defaults to 1.0 which yields sensor values in the range 0.0 to 1.0.
        /// </summary>
        /// <value>The scale.</value>
        double Scale { get; set; }

        /// <summary>
        ///     Obtains a fresh reading from the sensor. This will often be an IO bound operation
        ///     (e.g. if the sensor is on an I2C bus) and is a natural fit for an asynchronous
        ///     method. In general, try not to block on the result. Instead use the <c>await</c>
        ///     keyword or a task continuation to handle the result.
        /// </summary>
        /// <returns>
        ///     A task that will complete when the updated sensor value becomes available. Upon
        ///     completion, the task result will contain the updated value.
        /// </returns>
        /// <remarks>
        ///     Note: some sensors cannot produce readings on demand. In that case the operation
        ///     should complete immediately and yield the value from <see cref="LastValue" /> as the
        ///     result.
        /// </remarks>
        Task<double> UpdateValueAsync();
        }
    }