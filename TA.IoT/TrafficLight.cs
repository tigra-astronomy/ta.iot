﻿// This file is part of the TA.IoT project
// 
// File: TrafficLight.cs  Created: 2017-07-26@19:32
// Last modified: 2017-07-28@02:40

using System;
using System.Threading;
using System.Threading.Tasks;

namespace TA.IoT
    {
    /// <summary>
    ///     Monitors the value of some data stream and produces an alert condition based on preset
    ///     thresholds. This could be used to produce a "traffic light" indicator that displays
    ///     green/amber/red for alert conditions Ok, Warning and Alert. The alert condition has a
    ///     configurable hysteresis that maintains an alert condition until the data has remained
    ///     below the alert threshold for a period of time.
    /// </summary>
    public class TrafficLight : IDisposable
        {
        public enum AlertCondition
            {
            Ok,
            Warning,
            Alert
            }

        private readonly double alertThreshold;
        private readonly InterlockedBoolean enabled = new InterlockedBoolean();
        private readonly int hysteresis;
        private readonly INotifySensorValueChanged sensor;
        private readonly double warningThreshold;
        private TrafficLightStateBase currentState;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TrafficLight" /> class.
        /// </summary>
        /// <param name="sensor">The sensor that will provide the input data stream.</param>
        /// <param name="warningThreshold">The warning threshold.</param>
        /// <param name="alertThreshold">The alert threshold.</param>
        /// <param name="hysteresis">
        ///     The hysteresis delay on transitioning from Warning to Ok.
        /// </param>
        /// <exception cref="ArgumentException">
        ///     Warning threshold must be equal or greater than the Alert threshold
        /// </exception>
        public TrafficLight(INotifySensorValueChanged sensor, double warningThreshold, double alertThreshold,
            Timeout hysteresis)
            {
            if (alertThreshold < warningThreshold)
                throw new ArgumentException("Alert threshold must be equal or greater than the Warning threshold",
                    nameof(alertThreshold));
            this.sensor = sensor;
            this.warningThreshold = warningThreshold;
            this.alertThreshold = alertThreshold;
            this.hysteresis = hysteresis;
            currentState = new OkState(this);
            }

        /// <summary>
        ///     Gets the current alert condition.
        /// </summary>
        /// <value>The condition.</value>
        /// <exception cref="InvalidOperationException" accessor="get">
        ///     Thrown if the instance has never been enabled.
        /// </exception>
        public AlertCondition Condition
            {
            get
                {
                if (!enabledAtLeastOnce)
                    throw new InvalidOperationException(
                        "Call Enable() at least once before reading the Condition property");
                return currentState.Condition;
                }
            }

        /// <summary>
        ///     Gets a value indicating whether this <see cref="TrafficLight" /> is enabled.
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        public bool Enabled => enabled.Value;

        /// <summary>
        ///     Stops monitoring the input stream.
        /// </summary>
        public void Disable()
            {
            var wasEnabled = enabled.InterlockedExchange(false);
            if (wasEnabled)
                sensor.ValueChanged -= HandleSensorValueChanged;
            }

        /// <summary>
        ///     Begins monitoring the input stream. This must be called at least once or no output will be produced.
        /// </summary>
        public void Enable()
            {
            var wasEnabled = enabled.InterlockedExchange(true);
            if (!wasEnabled)
                sensor.ValueChanged += HandleSensorValueChanged;
            enabledAtLeastOnce = true;
            }

        private void HandleSensorValueChanged(object sender, SensorValueChangedEventArgs e)
            {
            if (!enabled.Value)
                return; // guard against events that fire after we are disabled
            currentState.Stimulus(e.Value);
            }

        private class AlertState : TrafficLightStateBase
            {
            public AlertState(TrafficLight owner) : base(owner)
                {
                Condition = AlertCondition.Alert;
                }

            public override void Stimulus(double value)
                {
                if (value < owner.warningThreshold)
                    StateTransition(new WarningState(owner));
                }
            }

        private class OkState : TrafficLightStateBase
            {
            public OkState(TrafficLight owner) : base(owner)
                {
                Condition = AlertCondition.Ok;
                }

            public override void Stimulus(double value)
                {
                if (value >= owner.alertThreshold)
                    StateTransition(new AlertState(owner));
                }
            }

        /// <summary>
        ///     This is the State Pattern
        /// </summary>
        private abstract class TrafficLightStateBase
            {
            protected readonly TrafficLight owner;

            public TrafficLightStateBase(TrafficLight owner)
                {
                this.owner = owner;
                }

            public AlertCondition Condition { get; set; }

            protected void StateTransition(TrafficLightStateBase destinationState)
                {
                owner.currentState = destinationState;
                }

            public abstract void Stimulus(double value);
            }

        private class WarningState : TrafficLightStateBase
            {
            private CancellationTokenSource hysteresisCancellation;

            public WarningState(TrafficLight owner) : base(owner)
                {
                Condition = AlertCondition.Warning;
                StartHysteresisDelay();
                }

            private void CancelHysteresis()
                {
                hysteresisCancellation?.Cancel();
                }

            private void HysteresisExpired(Task obj)
                {
                StateTransition(new OkState(owner));
                }

            private void StartHysteresisDelay()
                {
                hysteresisCancellation = new CancellationTokenSource();
                var hysteresisTask = Task.Delay(owner.hysteresis, hysteresisCancellation.Token);
                hysteresisTask.ConfigureAwait(false);
                hysteresisTask.ContinueWith(HysteresisExpired, TaskContinuationOptions.NotOnCanceled);
                }

            public override void Stimulus(double value)
                {
                if (value >= owner.alertThreshold)
                    {
                    CancelHysteresis();
                    StateTransition(new AlertState(owner));
                    return;
                    }
                if (value >= owner.warningThreshold)
                    {
                    CancelHysteresis();
                    StartHysteresisDelay();
                    }
                // else, we are waiting for the hysteresis delay to expire.
                }
            }

        #region IDisposable Pattern
        // The IDisposable pattern, as described at
        // http://www.codeproject.com/Articles/15360/Implementing-IDisposable-and-the-Dispose-Pattern-P


        /// <summary>
        ///     Finalizes this instance (called prior to garbage collection by the CLR)
        /// </summary>
        ~TrafficLight()
            {
            Dispose(fromUserCode: false);
            }

        public void Dispose()
            {
            Dispose(fromUserCode: true);
            GC.SuppressFinalize(this);
            }

        private bool disposed;
        private bool enabledAtLeastOnce;

        protected virtual void Dispose(bool fromUserCode)
            {
            if (!disposed)
                {
                if (fromUserCode)
                    {
                    // ToDo - Dispose managed resources (call Dispose() on any owned objects).
                    // Do not dispose of any objects that may be referenced elsewhere.
                    Disable();
                    }

                // ToDo - Release unmanaged resources here, if necessary.
                }
            disposed = true;

            // ToDo: Call the base class's Dispose(Boolean) method, if available.
            // base.Dispose(fromUserCode);
            }
        #endregion
        }
    }