// This file is part of the TA.IoT project
// 
// File: SensorValueChangedEventArgs.cs  Created: 2017-07-26@19:43
// Last modified: 2017-07-28@02:37

using System;

namespace TA.IoT
    {
    public sealed class SensorValueChangedEventArgs : EventArgs
        {
        public SensorValueChangedEventArgs(double newValue)
            {
            Value = newValue;
            }


        /// <summary>
        ///     Gets the updated sensor value, which should be in world units.
        /// </summary>
        /// <value>The value.</value>
        public double Value { get; }
        }
    }