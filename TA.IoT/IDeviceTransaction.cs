// This file is part of the TA.IoT project
// 
// File: IDeviceTransaction.cs  Created: 2017-07-28@02:14
// Last modified: 2017-07-28@02:32

namespace TA.IoT
    {
    /// <summary>
    ///     Data to be transmitted and corresponding data received from a device.
    /// </summary>
    public interface IDeviceTransaction
        {
        /// <summary>
        ///     The receive buffer which, if present, will be of the correct size to hold the expected response.
        /// </summary>
        /// <value>The read transaction.</value>
        byte[] ReceiveBuffer { get; }

        /// <summary>
        ///     The byte stream to be transmitted, if any.
        /// </summary>
        /// <value>The write transaction.</value>
        byte[] TransmitBuffer { get; }
        }
    }