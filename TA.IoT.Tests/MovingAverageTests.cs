﻿// This file is part of the TA.UWP project
// 
// File: MovingAverageTests.cs  Created: 2017-07-26@21:52
// Last modified: 2017-07-26@23:07

using Machine.Specifications;
using Xunit;

namespace TA.IoT.Tests
    {
    public class MovingAverageTests
        {
        [Fact]
        public void AverageShouldChangeAsTheInputValuesAreAdded()
            {
            var averager = new MovingAverage(10);
            averager.AddSample(1.0);
            averager.Average.ShouldBeCloseTo(1.0, double.Epsilon);
            averager.AddSample(1.0);
            averager.Average.ShouldBeCloseTo(1.0, double.Epsilon);
            averager.AddSample(2.0);
            averager.Average.ShouldBeCloseTo(4.0 / 3.0, double.Epsilon);
            averager.AddSample(2.0);
            averager.Average.ShouldBeCloseTo(1.5, double.Epsilon);
            // Fill the sample window and check that the average equals the input
            averager.AddSample(10.0);
            averager.AddSample(10.0);
            averager.AddSample(10.0);
            averager.AddSample(10.0);
            averager.AddSample(10.0);
            averager.AddSample(10.0);
            averager.AddSample(10.0);
            averager.AddSample(10.0);
            averager.AddSample(10.0);
            averager.AddSample(10.0);
            averager.Average.ShouldBeCloseTo(10.0, double.Epsilon);
            averager.AddSample(100.0);
            // Average should now be (9 * 10 + 100) / 10 = 19
            averager.Average.ShouldBeCloseTo(19.0, double.Epsilon);
            }

        [Fact]
        public void InitializingToAValueSetsTheAverage()
            {
            var averager = new MovingAverage(10);
            averager.Clear(99);
            averager.Average.ShouldEqual(99);
            }
        }
    }