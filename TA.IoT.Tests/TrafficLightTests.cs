﻿// This file is part of the TA.UWP project
// 
// File: TrafficLightTests.cs  Created: 2017-07-26@23:57
// Last modified: 2017-07-27@00:10

using System;
using System.Threading.Tasks;
using Machine.Specifications;
using Xunit;

namespace TA.IoT.Tests
    {
    public class TrafficLightTests
        {
        private TrafficLight trafficLight;
        private FakeSensor sensor;
        private const double WarningThreshold = 10.0;
        private const double AlertThreshold = 20.0;
        Timeout hysteresis = Timeout.FromMilliseconds(100);

        public TrafficLightTests()
            {
            sensor = new FakeSensor();
            trafficLight = new TrafficLight(sensor, WarningThreshold, AlertThreshold, hysteresis);
            }

        [Fact]
        public async Task GivesWarningConfitionAsSoonAsWarningThresholdExceeded()
            {
            trafficLight.Enable();
            // Sensor value is nominal, state should be OK
            sensor.RaiseSensorValueChanged(0);
            trafficLight.Condition.ShouldEqual(TrafficLight.AlertCondition.Ok);
            // Warning threshold doesn't have any effect, state goes directly from OK -> Alert
            sensor.RaiseSensorValueChanged(WarningThreshold);
            trafficLight.Condition.ShouldEqual(TrafficLight.AlertCondition.Ok);
            // As soon as we hit the alert threshold, then obviously we should get an alert
            sensor.RaiseSensorValueChanged(AlertThreshold);
            trafficLight.Condition.ShouldEqual(TrafficLight.AlertCondition.Alert);
            // Sensor value returns below the warning threshold
            sensor.RaiseSensorValueChanged(WarningThreshold-0.000000001);
            trafficLight.Condition.ShouldEqual(TrafficLight.AlertCondition.Warning);
            // After the hysteress time, the state should return to OK
            await Task.Delay(hysteresis.TimeSpan).ConfigureAwait(false);
            // We need this additional delay otherwise the trafficLight doesn't have time
            // to update. This is a bit brittle because it relies on task ordering.
            await Task.Delay(1).ConfigureAwait(false);
            trafficLight.Condition.ShouldEqual(TrafficLight.AlertCondition.Ok);
            }
        internal class FakeSensor : INotifySensorValueChanged
            {
            public event EventHandler<SensorValueChangedEventArgs> ValueChanged;

            public double ValueChangedThreshold { get; set; }

            internal void RaiseSensorValueChanged(double value)
                {
                var eventArgs = new SensorValueChangedEventArgs(value);
                ValueChanged?.Invoke(this, eventArgs);
                }
            }
    }
}