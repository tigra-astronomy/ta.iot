﻿// This file is part of the TA.IoT project
// 
// File: InterpolatedLookup.cs  Created: 2017-07-26@19:32
// Last modified: 2017-07-28@02:37

using System;

namespace TA.IoT
    {
    /// <summary>
    ///     Implements a value lookup table with linear interpolation between values.
    /// </summary>
    public sealed class InterpolatedLookup
        {
        private readonly int count;
        private readonly double highestKey;
        private readonly double[] keys;
        private readonly int lastIndex;
        private readonly double lowestKey;
        private readonly double[] values;

        public InterpolatedLookup(double[] keys, double[] values)
            {
            this.keys = keys ?? throw new ArgumentNullException(nameof(keys));
            this.values = values ?? throw new ArgumentNullException(nameof(values));
            if (keys.Length < 1) throw new ArgumentException("Array cannot be empty", nameof(keys));
            if (values.Length < 1) throw new ArgumentException("Array cannot be empty", nameof(values));
            if (values.Length != keys.Length)
                throw new ArgumentException(
                    $"{nameof(keys)} and {nameof(values)} must contain the same number of elements");
            count = keys.Length;
            lastIndex = count - 1;
            lowestKey = keys[0];
            highestKey = keys[lastIndex];
            }

        public double Lookup(double lookup)
            {
            // Shortcut: If table contains only 1 element, return that value.
            if (count == 1)
                return values[0];
            // Shortcut: If the lookup value is off the end of the table, return an end value.
            if (lookup <= lowestKey) return values[0];
            if (lookup >= highestKey) return values[lastIndex];
            /*
             * At this point, we know that the lookup value is >= first key and <= last key and not equal to any of the keys.
             * Find the key pair that surrounds the lookup value, such that k1 <= lookup < k2
             */
            var index = 0;
            while (keys[index] < lookup)
                {
                ++index;
                }
            var upperKey = keys[index];
            var upperValue = values[index];
            --index;
            var lowerKey = keys[index];
            var lowerValue = values[index];
            var keySpan = upperKey - lowerKey;
            var interpolatedDistance = (lookup - lowerKey) / keySpan; // fraction of unity
            var valueSpan = upperValue - lowerValue;
            var interpolatedValue = lowerValue + valueSpan * interpolatedDistance;
            return interpolatedValue;
            }
        }
    }