﻿// This file is part of the TA.IoT project
// 
// File: DeviceTransaction.cs  Created: 2017-07-28@02:36
// Last modified: 2017-07-28@02:36

namespace TA.IoT
    {
    /// <summary>
    ///     Represents an atomic send-receive transaction with some addressable device.
    /// </summary>
    public class DeviceTransaction : IDeviceTransaction
        {
        protected internal DeviceTransaction(byte[] transmitBuffer, byte[] receiveBuffer)
            {
            TransmitBuffer = transmitBuffer;
            ReceiveBuffer = receiveBuffer;
            }

        /// <summary>
        ///     The receive buffer which, if present, will be of the correct size to hold the expected response.
        /// </summary>
        /// <value>The read transaction.</value>
        public byte[] ReceiveBuffer { get; protected set; }

        /// <summary>
        ///     The byte stream to be transmitted, if any.
        /// </summary>
        /// <value>The write transaction.</value>
        public byte[] TransmitBuffer { get; protected set; }
        }
    }