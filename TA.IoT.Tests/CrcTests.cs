﻿using System.Linq;
using Machine.Specifications;
using Xunit;

namespace TA.IoT.Tests
{
    public class CrcTests
    {
        /* Sample data taken from data sheet of Melexis MLX90614 */
    [Theory]
    [InlineData(Crc8.SMBusPolynomial, 0x30, new byte[] { 0xB4, 0x07, 0xB5, 0xD2, 0x3A })]
    [InlineData(Crc8.SMBusPolynomial, 0x00, new byte[] { 0xB4, 0x07, 0xB5, 0xD2, 0x3A, 0x30 })]
    [InlineData(Crc8.SMBusPolynomial, 0x48, new byte[] { 0xB4, 0x22, 0x07, 0xC8})]
    public void Crc8AlgorithmsShouldGenerateTheExpectedChecksum(int algorithm, byte expectedChecksm, byte[] sourceData)
        {
        var crc = new Crc8(algorithm);
        var actualChecksum = crc.Checksum(sourceData).Single();
        actualChecksum.ShouldEqual(expectedChecksm);
        }
    }
}
