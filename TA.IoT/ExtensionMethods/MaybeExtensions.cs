// This file is part of the TA.IoT project
// 
// File: MaybeExtensions.cs  Created: 2017-07-26@02:05
// Last modified: 2017-07-28@02:32

using System.Linq;

namespace TA.IoT.ExtensionMethods
    {
    public static class MaybeExtensions
        {
        public static bool None<T>(this Maybe<T> maybe)
            {
            if (maybe == null || maybe == Maybe<T>.Empty)
                return true;
            return !maybe.Any();
            }
        }
    }