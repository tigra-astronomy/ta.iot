﻿// This file is part of the TA.IoT project
// 
// File: BitManipulationExtensions.cs  Created: 2017-07-28@20:53
// Last modified: 2017-07-28@21:00

using System;

namespace TA.IoT.ExtensionMethods
    {
    internal static class BitManipulationExtensions
        {
        private const ulong Unity = 1;

        /// <summary>
        ///     Returns a boolean value corresponding to the value at the specified bit position.
        /// </summary>
        /// <param name="register">The register, an unsigned integer, containing bit values.</param>
        /// <param name="bitPosition">
        ///     The bit position to be tested, where bit 0 is the least
        ///     significant bit.
        /// </param>
        /// <returns>
        ///     A boolean value corresponding to the bit at the specified bit position.
        /// </returns>
        public static bool Bit(this ulong register, int bitPosition)
            {
            if (bitPosition < 0 || bitPosition > 63)
                throw new ArgumentOutOfRangeException(nameof(bitPosition), "Valid bit positions are 0..63");
            var mask = Unity << bitPosition;
            return (register & mask) != 0;
            }
        }
    }