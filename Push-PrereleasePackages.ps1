$feed = "https://www.myget.org/F/tigra-astronomy/api/v2/package"
$symbolFeed = "https://www.myget.org/F/tigra-astronomy/symbols/api/v2/package"
$configuration = "Debug"

dotnet pack .\TA.IoT\TA.IoT.csproj --include-symbols --include-source --configuration $configuration
Push-Location .\TA.IoT\bin\$configuration
$packages = Get-ChildItem -Filter *.nupkg
foreach ($package in $packages)
{
    if ($package.Name -like "*.symbols.nupkg")
    {
        # Do nothing
        #dotnet nuget push $package --symbol-source $symbolFeed 
    }
    else 
    {
        dotnet nuget push $package --source $feed --symbol-source $symbolFeed
    }
}
Pop-Location