// This file is part of the TA.IoT project
// 
// File: Maybe.cs  Created: 2017-07-26@02:04
// Last modified: 2017-07-28@02:32

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TA.IoT
    {
    /// <summary>
    ///     Represents an object that may or may not have a value (strictly, a collection of zero or
    ///     one elements). Use LINQ expression
    ///     <c>maybe.Any()</c> to determine if there is a value. Use LINQ expression
    ///     <c>maybe.Single()</c> to retrieve the value.
    /// </summary>
    /// <typeparam name="T">The type of the item in the collection.</typeparam>
    /// <remarks>
    ///     This type almost completely eliminates any need to return
    ///     <c>null</c> or deal with possibly null references, which makes code cleaner and more
    ///     clearly expresses the intent of 'no value' versus 'error'.  The value of a Maybe cannot
    ///     be <c>null</c>, because
    ///     <c>null</c> really means 'no value' and that is better expressed by using
    ///     <see cref="Empty" />.
    /// </remarks>
    public class Maybe<T> : IEnumerable<T>
        {
        private readonly IEnumerable<T> values;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Maybe{T}" /> with no value.
        /// </summary>
        private Maybe()
            {
            values = Array.Empty<T>();
            }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Maybe{T}" /> with a value.
        /// </summary>
        /// <param name="value">The value.</param>
        public Maybe(T value)
            {
            values = new[] {value};
            }

        /// <summary>
        ///     Gets an instance that does not contain a value.
        ///     We always return the same empty instance so it is reference-equal to all other empty instances.
        /// </summary>
        /// <value>The empty instance.</value>
        public static Maybe<T> Empty { get; } = new Maybe<T>();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        /// <summary>
        ///     Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the
        ///     collection.
        /// </returns>
        public IEnumerator<T> GetEnumerator() => values.GetEnumerator();

        /// <summary>
        ///     Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
            {
            if (Equals(Empty))
                return "{no value}";
            return this.Single().ToString();
            }
        }
    }