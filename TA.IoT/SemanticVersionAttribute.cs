﻿// This file is part of the TA.IoT project
// 
// File: SemanticVersionAttribute.cs  Created: 2017-07-26@02:04
// Last modified: 2017-07-28@02:37

using System;
using System.Runtime.Serialization;

namespace TA.IoT
    {
    /// <summary>
    ///     Semantic Version attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly)]
    [DataContract]
    public sealed class SemanticVersionAttribute : Attribute
        {
        public SemanticVersionAttribute(string semanticNumber)
            {
            Version = new SemanticVersion(semanticNumber);
            }

        [DataMember]
        public SemanticVersion Version { get; }
        }
    }