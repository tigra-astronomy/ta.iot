using System;
using Machine.Specifications;
using Xunit;

namespace TA.IoT.Tests
{
    public class TimeoutTests
    {
        [Fact]
        void TimeIsUniformRegardlessOfUnits()
        {
            var oneSecondTimeout = Timeout.FromSeconds(1);
            var oneSecondTimeSpam = TimeSpan.FromSeconds(1);
            oneSecondTimeout.TimeSpan.ShouldEqual(oneSecondTimeSpam);
            oneSecondTimeout.Ticks.ShouldEqual(TimeSpan.TicksPerSecond);
            oneSecondTimeout.Milliseconds.ShouldEqual(1000);
        }
    }
}
