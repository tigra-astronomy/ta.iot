﻿// This file is part of the TA.IoT project
// 
// File: IComputeChecsum.cs  Created: 2017-07-26@19:32
// Last modified: 2017-07-28@02:32

namespace TA.IoT
    {
    public interface IComputeChecsum
        {
        byte[] Checksum(params byte[] value);
        }
    }