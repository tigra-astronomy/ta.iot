// This file is part of the TA.IoT project
// 
// File: CadencedItem.cs  Created: 2017-07-28@20:40
// Last modified: 2017-07-28@23:41

using System;
using System.Threading.Tasks;

namespace TA.IoT
    {
    public sealed partial class SynchronizedCadenceGenerator
        {
        private class CadencedItem
            {
            private readonly ulong continuingCadence;

            private readonly ulong initialCadence;

            public CadencedItem(Action turnOn, Action turnOff, ulong initialCadence, ulong continuingCadence,
                bool captureSynchronizationContext = true, string name = null)
                {
                TurnOn = turnOn;
                TurnOff = turnOff;
                this.initialCadence = initialCadence;
                this.continuingCadence = continuingCadence;
                Name = name ?? Guid.NewGuid().ToString();
                Scheduler = captureSynchronizationContext
                    ? TaskScheduler.FromCurrentSynchronizationContext()
                    : TaskScheduler.Default;
                }

            public ulong Cadence { get; set; }

            internal bool CurrentState { get; set; }

            public string Name { get; }

            internal TaskScheduler Scheduler { get; }

            internal Action TurnOff { get; }

            internal Action TurnOn { get; }
            }
        }
    }