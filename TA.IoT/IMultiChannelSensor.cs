﻿// This file is part of the TA.IoT project
// 
// File: IMultiChannelSensor.cs  Created: 2017-07-29@15:54
// Last modified: 2017-07-29@16:00

using System.Threading.Tasks;

namespace TA.IoT
    {
    public interface IMultiChannelSensor
        {
        /// <summary>
        ///     Gets the channel count.
        /// </summary>
        /// <value>The channel count.</value>
        uint ChannelCount { get; }

        /// <summary>
        ///     Gets a sensor channel.
        /// </summary>
        /// <param name="channel">The channel number to be accessed.</param>
        /// <returns>
        ///     An <see cref="ISensorChannel" /> that can be used to access the channel's value stream.
        /// </returns>
        ISensorChannel this[uint channel] { get; }

        /// <summary>
        ///     Asynchronously obtains updated values from all channels.
        /// </summary>
        /// <returns>
        ///     The returned task completes when all channel updates have completed and have valid data
        ///     available.
        /// </returns>
        Task UpdateAllChannelsAsync();
        }
    }